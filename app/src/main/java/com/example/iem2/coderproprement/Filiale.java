package com.example.iem2.coderproprement;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by iem on 12/10/15.
 */
public class Filiale {
    String nom;
    Bitmap logo;
    int chiffreAffaire;
    String origine;
    int nbEmploye;
    Date dateDeRachat;

    public Filiale(String nom, Bitmap logo, int chiffreAffaire, String origine, int nbEmploye, Date dateDeRachat) {
        this.nom = nom;
        this.logo = logo;
        this.chiffreAffaire = chiffreAffaire;
        this.origine = origine;
        this.nbEmploye = nbEmploye;
        this.dateDeRachat = dateDeRachat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Bitmap getLogo() {
        return logo;
    }

    public void setLogo(Bitmap logo) {
        this.logo = logo;
    }

    public int getChiffreAffaire() {
        return chiffreAffaire;
    }

    public void setChiffreAffaire(int chiffreAffaire) {
        this.chiffreAffaire = chiffreAffaire;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public int getNbEmploye() {
        return nbEmploye;
    }

    public void setNbEmploye(int nbEmploye) {
        this.nbEmploye = nbEmploye;
    }

    public Date getDateDeRachat() {
        return dateDeRachat;
    }

    public void setDateDeRachat(Date dateDeRachat) {
        this.dateDeRachat = dateDeRachat;
    }
}
