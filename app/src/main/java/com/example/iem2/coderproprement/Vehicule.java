package com.example.iem2.coderproprement;

/**
 * Created by iem on 12/10/15.
 */
public abstract class Vehicule {
    Filiale marque;
    String modele;
    int prix;

    public Vehicule(Filiale marque, String modele, int prix) {
        this.marque = marque;
        this.modele = modele;
        this.prix = prix;
    }

    public Filiale getMarque() {
        return marque;
    }

    public void setMarque(Filiale marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
}
